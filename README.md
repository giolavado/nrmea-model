# NRMEA Model

NRMEA-Model is a JAVA project that contains fragment-based models for [VEGA](https://www.vegahub.eu/portfolio-item/vega-qsar/) platform. The implemented models give a qualitative prediction for thyroid hormone receptor alpha/beta. The models are a re-implementation of models developed by the group of Prof. Wei Shi, University of Nanjing [NRMEA tool](https://www.vegahub.eu/portfolio-item/vega-nrmea/).

## Installation

### Installing dependencies

NRMEA-Model project is developed in JAVA using Apache Netbeans 12.5 as IDE. It uses the following dependences:

* Open JDK 11

* cdk-1.4.9-pruned.jar

* insilicoCore.jar

## Reference

Further details about the implemented models can be found in [VEGA NRMEA](https://www.vegahub.eu/portfolio-item/vega-nrmea/). 

## Contributing and credits

NRMEA-Model project was originally created by Giovanna Janet Lavado at [Laboratory of Environmental Chemistry and Toxicology](https://www.marionegri.it/laboratori/laboratorio-chimica-e-tossicologia-dellambiente), Department of Environmental Health Sciences, Istituto di Ricerche Farmacologiche Mario Negri - IRCCS.

## License
NRMEA-Model is licensed under [GNU GPLv3](https://choosealicense.com/licenses/gpl-3.0/)