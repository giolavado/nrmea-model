package insilico.core.nrmea.test;

import insilico.core.exception.GenericFailureException;
import insilico.core.exception.InvalidMoleculeException;
import insilico.core.nrmea.NRMEAModel;
import insilico.core.molecule.InsilicoMolecule;
import insilico.core.molecule.conversion.SmilesMolecule;
import java.util.logging.Level;
import java.util.logging.Logger;
import insilico.core.nrmea.model.tr.THReceptorAlpha;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

/**
 *
 * @author Giovanna Lavado
 */
public class TestTRAlphaModel {

    /**
     * 
     * @param args
     * @throws InvalidMoleculeException
     * @throws GenericFailureException
     * @throws Exception 
     */
    public static void main(String[] args) throws InvalidMoleculeException, GenericFailureException, Exception {
        //System.out.println("Hello Gio'");

        test();
        
        //String smi_file = args[0];
        //String preds_file = args[1];
        
        //testAllSMILES(smi_file, preds_file);
        
    }
     
    public static void testAllSMILES(String smi_file, String preds_file) throws Exception{
        NRMEAModel THRAlphamodel = new THReceptorAlpha();
  
        BufferedReader reader = new BufferedReader(new FileReader(smi_file));
        BufferedWriter writer = new BufferedWriter(new FileWriter(preds_file));
        String line;
        InsilicoMolecule imol;
        
        while ((line = reader.readLine()) != null){
            imol = null;
               
            // convert the given SMILES string to a InsilicoMolecule object
            // which contains the molecule as a graph, together with other 
            // chemical information
            imol = SmilesMolecule.Convert(line.trim());

            // if there is a parsing error (the SMILES string is not correct)
            // throws an exception as no further elaborations can be done
            if (!imol.IsValid()) 
                //writer.write(line.trim() + "\t" + "-999"+"\r\n");
                throw new GenericFailureException(imol.GetErrors().GetMessages());
            //molecule = imol.GetStructure();        
            
            Short pred = THRAlphamodel.Predict(imol);
            System.out.println(pred);
            writer.write(line.trim() + "\t" + Short.toString(pred)+"\t"+ !imol.IsValid()+ "\r\n");
            

        }
        reader.close();
        writer.close();
    }
    
    
    public static void test() throws Exception{

        THReceptorAlpha THRAlphaModel = new THReceptorAlpha();
        InsilicoMolecule imol = null;
            
        try {
        // example1 agonist
        // original SMILES
        //String smi = "Cc1cc(NC(=O)CC(=O)O)c2CCCc2c1Oc3ccc(O)c(CCc4cccc(O)c4)c3"; 
        // vega SMILES  
        //String smi = "O=C(O)CC(=O)Nc3cc(c(Oc1ccc(O)c(c1)CCc2cccc(O)c2)c4c3CCC4)C"; //ok, agonist

        // original SMILES
        //String smi = "CC(C)C1=CC(=NNC1=O)Sc2c(Cl)cc(CC(=O)O)cc2Cl"; // example2 agosnist
        // vega SMILES  
        //String smi = "O=C(O)Cc1cc(c(c(c1)Cl)SC2=NNC(=O)C(=C2)C(C)C)Cl"; // ok, agonist
        
        // vega SMILES
        // no primary: ND -> inactive
        //String smi = "C#CC3(O)(CCC2C4CCc1cc(OC)ccc1C4(CCC23(C)))"; // ok, inactive

// vega SMILES to test
// no primary: ND -> inactive        
/*        
C#CC(O)(C)C // ok
C#CC(O)(C)CC //ok
C#CC(O)(C)CC(C)C //ok
C#CC(O)C(CC)CCCC
C#CC(O)CCCCC
C#CC1(O)(CCCCC1)
C#CC3(O)(CCC2C4CCc1cc(OC)ccc1C4(CCC23(C)))

*/
        
        //335	Molecule 335	N#CC1=NN(C(=O)NC1(=O))c2cc(c(c(c2)Cl)CC3=NN(C(=O)C(=C3)C(C)C)C)Cl	agonist
        //String smi = "N#CC1=NN(C(=O)NC1(=O))c2cc(c(c(c2)Cl)CC3=NN(C(=O)C(=C3)C(C)C)C)Cl"; //ok, agonist

        //1262	Molecule 1262	O=C(NC23(CC1CC(CC(C1)C2)C3))c5cnc(c4ccc(c(c4)[N+](=O)[O-])S(=O)(=O)C)s5	antagonist
        String smi = "O=C(NC23(CC1CC(CC(C1)C2)C3))c5cnc(c4ccc(c(c4)[N+](=O)[O-])S(=O)(=O)C)s5"; // ok, antagonist

        System.out.println(smi);
        imol = SmilesMolecule.Convert(smi);

        // if there is a parsing error (the SMILES string is not correct)
        // throws an exception as no further elaborations can be done
        if (!imol.IsValid())
            throw new GenericFailureException(imol.GetErrors().GetMessages());
        
        Short pred = THRAlphaModel.Predict(imol);
            
        System.out.println("pred:"+pred);
            
        } catch (Exception ex) {
            Logger.getLogger(TestTRAlphaModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
