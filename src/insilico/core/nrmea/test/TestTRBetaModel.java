package insilico.core.nrmea.test;

import insilico.core.exception.GenericFailureException;
import insilico.core.exception.InvalidMoleculeException;
import insilico.core.molecule.InsilicoMolecule;
import insilico.core.molecule.conversion.SmilesMolecule;
import insilico.core.nrmea.NRMEAModel;
import insilico.core.nrmea.model.tr.THReceptorBeta;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Giovanna Lavado
 */
public class TestTRBetaModel {
    public static void main(String[] args) throws InvalidMoleculeException, GenericFailureException, Exception {
       
        testTRBetaModel();
        //testLoadFileSMILES(args[0],args[1]);
        
    }
    
    public static void testLoadFileSMILES(String smi_file, String preds_file) throws FileNotFoundException, IOException, Exception{
     NRMEAModel THRBetaModel = new THReceptorBeta();
        
        BufferedReader reader = new BufferedReader(new FileReader(smi_file));
        BufferedWriter writer = new BufferedWriter(new FileWriter(preds_file));
        String line;
        InsilicoMolecule imol;
        
        while ((line = reader.readLine()) != null){
            imol = null;
            
           
            // convert the given SMILES string to a InsilicoMolecule object
            // which contains the molecule as a graph, together with other 
            // chemical information
            imol = SmilesMolecule.Convert(line.trim());

            // if there is a parsing error (the SMILES string is not correct)
            // throws an exception as no further elaborations can be done
            if (!imol.IsValid()) 
                throw new GenericFailureException(imol.GetErrors().GetMessages());
            
            Short pred = THRBetaModel.Predict(imol);
            
            System.out.println(pred);
            writer.write(line.trim() + "\t" + Short.toString(pred)+"\t"+ !imol.IsValid()+ "\r\n");
            
            
        }
        
        reader.close();
        writer.close();
    }
    
    public static void testTRBetaModel() throws Exception{
        THReceptorBeta THRBetaModel = new THReceptorBeta();
        InsilicoMolecule imol = null;
    
        //String smi = "O=C(NC23(CC1CC(CC(C1)C2)C3))c5cnc(c4ccc(c(c4)[N+](=O)[O-])S(=O)(=O)C)s5";
        String smi = "N#CC1=NN(C(=O)NC1(=O))c2cc(c(c(c2)Cl)CC3=NN(C(=O)C(=C3)C(C)C)C)Cl";
        
        System.out.println(smi);
        imol = SmilesMolecule.Convert(smi);

        // if there is a parsing error (the SMILES string is not correct)
        // throws an exception as no further elaborations can be done
        if (!imol.IsValid())
            try {
                throw new GenericFailureException(imol.GetErrors().GetMessages());
        } catch (GenericFailureException ex) {
            Logger.getLogger(TestTRAlphaModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Short pred = THRBetaModel.Predict(imol);
            
        System.out.println("pred:"+pred);
        
    }
}
