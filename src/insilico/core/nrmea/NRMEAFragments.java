package insilico.core.nrmea;

import insilico.core.exception.GenericFailureException;
import insilico.core.molecule.InsilicoMolecule;
import insilico.core.molecule.tools.CustomQueryMatcher;
import org.openscience.cdk.exception.CDKException;
import java.util.ArrayList;

/**
 *
 * @author Giovanna Lavado
 * @author Cosimo Toma
 */
public class NRMEAFragments {
    
    private final ArrayList<Fragment> fragmentsList;

    
    public NRMEAFragments(ArrayList<Fragment> frags) {
        this.fragmentsList = frags;        
    }
    
        
    public boolean match(InsilicoMolecule mol) throws GenericFailureException {
        
        try {
            CustomQueryMatcher Matcher = new CustomQueryMatcher(mol);

            // check for matching, returns true when it finds the first
            // matching fragments - false otherwise
            for (Fragment curFrag : fragmentsList) 
                if (Matcher.matches(curFrag.getQuery())) 
                    return true;

            return false;
        } catch (CDKException ex) {
            throw new GenericFailureException ("Error during SMARTS matching: " + ex.getMessage());
        }
   
    }
            
}
