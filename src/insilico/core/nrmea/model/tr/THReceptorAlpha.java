package insilico.core.nrmea.model.tr;

import insilico.core.molecule.InsilicoMolecule;
import insilico.core.nrmea.NRMEAModel;
import insilico.core.nrmea.NRMEAFragments;

/**
 *
 * @author Giovanna Lavado
 * @author Cosimo Toma
 */
public class THReceptorAlpha extends NRMEAModel {
    
    //the arrays depend on the specific model  
    protected NRMEAFragments typeOneFragmentList = null; // type1
    protected NRMEAFragments typeTwoFragmentList = null; // type2
    protected NRMEAFragments typeOneOneFragmentList = null; //type1-1
    protected NRMEAFragments typeTwoOneFragmentList = null; //type2-1
    protected NRMEAFragments typeOneOneOneFragmentList = null; //type1-1-1
    
    private static final String TYPE_1 = "type1"; 
    private static final String TYPE_2 = "type2";
    private static final String TYPE_11 = "type1-1";
    private static final String TYPE_21 = "type2-1";
    private static final String TYPE_111 = "type1-1-1";    
    
    
    public THReceptorAlpha() throws Exception {
        super("/insilico/core/nrmea/model/tr/fragments_alpha.txt");
        
    }
  
    
    /**
     * Calculate the prediction of the specific model.
     * @param mol
     * @return 1=agonist, 2=antagonist, 4=ND
     * @throws Exception 
     */
    @Override
    protected int calculatePrediction(InsilicoMolecule mol) throws Exception {
        //System.out.println("calculatePrediction");
        
        int retval = 4; // inactive o ND? sembra ND
        
        // controllo se SMILES fa match con un frammento primario
        // da vedere se alla fine serve restituire anche ND per le predizioni
        
        boolean isprimary = PrimaryFragsList.match(mol);
        if (!isprimary) 
            return retval;
        
        // da inizializzare tutti gli array in una volta sola oppure inizializzarli solo se necessario
        //initFragsList(); // qui oppure in costruttore
        
        boolean istype1 = typeOneFragmentList.match(mol);
        
        // se e' primario
        
        if(istype1){ // se tipo1

            boolean istype11 = typeOneOneFragmentList.match(mol);

            if (istype11) {

                boolean istype111 = typeOneOneOneFragmentList.match(mol);

                if (istype111) 
                    retval = 1;
                else
                    retval = 2;
            }    
        } else { // se tipo2
         
            boolean istype21 = typeTwoOneFragmentList.match(mol);

            if (istype21) 
                retval = 1;
        }

        return retval;
    }

    
    @Override
    protected void initFragsList() throws Exception{
        if (this.typeOneFragmentList == null)
            this.typeOneFragmentList = new NRMEAFragments(getFragmentListOSubType(TYPE_1));
        
        if (this.typeTwoFragmentList == null)
            typeTwoFragmentList = new NRMEAFragments(getFragmentListOSubType(TYPE_2));
        
        if (this.typeOneOneFragmentList == null)
            typeOneOneFragmentList = new NRMEAFragments(getFragmentListOSubType(TYPE_11));
        
        if (this.typeTwoOneFragmentList == null)
            typeTwoOneFragmentList = new NRMEAFragments(getFragmentListOSubType(TYPE_21));
         
        if (this.typeOneOneOneFragmentList == null)
            typeOneOneOneFragmentList = new NRMEAFragments(getFragmentListOSubType(TYPE_111));
        
    }
}
