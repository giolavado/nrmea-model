package insilico.core.nrmea.model.tr;

import insilico.core.molecule.InsilicoMolecule;
import insilico.core.nrmea.NRMEAFragments;
import insilico.core.nrmea.NRMEAModel;

/**
 *
 * @author Giovanna
 */
public class THReceptorBeta extends NRMEAModel{

    //the arrays depend on the specific model  
    protected NRMEAFragments typeOneFragmentList = null; // type1
    protected NRMEAFragments typeTwoFragmentList = null; // type2
    protected NRMEAFragments typeOneOneFragmentList = null; //type1-1
    protected NRMEAFragments typeTwoOneFragmentList = null; //type2-1
    protected NRMEAFragments typeOneOneOneFragmentList = null; //type1-1-1
    protected NRMEAFragments typeTwoOneOneFragmentList = null; //type2-1-1
    
    // constants
    private static final String TYPE_1 = "type1"; 
    private static final String TYPE_2 = "type2";
    private static final String TYPE_11 = "type1-1";
    private static final String TYPE_21 = "type2-1";
    private static final String TYPE_111 = "type1-1-1";
    private static final String TYPE_211 = "type2-1-1";
    
    /**
     * Constructor
     * 
     */
    public THReceptorBeta() {
        super("/insilico/core/nrmea/model/tr/fragments_beta.txt");
    }

    /**
     * Calculate the prediction of the specific model.
     * @param mol
     * @return 1=agonist, 2=antagonist, 4=ND
     * @throws Exception 
     * 
     */
    @Override
    protected int calculatePrediction(InsilicoMolecule mol) throws Exception {
        int retval = 4;
        
        // check with a primary fragment
        boolean isprimary = PrimaryFragsList.match(mol);
        if (!isprimary) 
            return retval;
        
        boolean istype1 = typeOneFragmentList.match(mol);
        
        // if primary 
        if(istype1){ // if type1

            boolean istype11 = typeOneOneFragmentList.match(mol);

            if (istype11) {

                boolean istype111 = typeOneOneOneFragmentList.match(mol);

                if (istype111) 
                    retval = 1; // agonist
                else
                    retval = 2; // antagonist
            }    
        } else { // if type2
         
            boolean istype21 = typeTwoOneFragmentList.match(mol);

            if (istype21){ // if type21
            
                boolean istype211 = typeTwoOneOneFragmentList.match(mol);
                
                if (istype211)
                    retval = 1; // agonist
                else retval = 2; // antagonist
            }
        }

        return retval;
        
    }

    /**
     * 
     * @throws Exception 
     */
    @Override
    protected void initFragsList() throws Exception {
        if (this.typeOneFragmentList == null)
            this.typeOneFragmentList = new NRMEAFragments(getFragmentListOSubType(TYPE_1));
        
        if (this.typeTwoFragmentList == null)
            typeTwoFragmentList = new NRMEAFragments(getFragmentListOSubType(TYPE_2));
        
        if (this.typeOneOneFragmentList == null)
            typeOneOneFragmentList = new NRMEAFragments(getFragmentListOSubType(TYPE_11));
        
        if (this.typeTwoOneFragmentList == null)
            typeTwoOneFragmentList = new NRMEAFragments(getFragmentListOSubType(TYPE_21));
         
        if (this.typeOneOneOneFragmentList == null)
            typeOneOneOneFragmentList = new NRMEAFragments(getFragmentListOSubType(TYPE_111));

        if (this.typeTwoOneOneFragmentList == null)
            typeTwoOneOneFragmentList = new NRMEAFragments(getFragmentListOSubType(TYPE_211));
        
    }


    
}
