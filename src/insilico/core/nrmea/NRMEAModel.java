package insilico.core.nrmea;

import insilico.core.exception.InitFailureException;
import insilico.core.molecule.InsilicoMolecule;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

/**
 *
 * @author Giovanna Lavado
 * @author Cosimo Toma
 */
public abstract class NRMEAModel {

    public static final short UNDEFINED = -1;
    public static final short INACTIVE = 0;
    public static final short AGONIST = 1;
    public static final short ANTAGONIST = 2;
    public static final short A_ANTA = 3; 
    
    protected final String FragsSource;
    protected ArrayList<Fragment> FragsList; // fragment list: primary, secondary, tertiary and so on

    static final String PRIMARY = "primary"; // gli specifici modelli restituiscono ND quando gli SMILES no fanno match con i frammenti primari 

    protected NRMEAFragments PrimaryFragsList;
    
    /**
     * Constructor
     * 
     * @param FragsSource 
     */
    public NRMEAModel(String FragsSource) { // to initialize data structures
        this.FragsSource = FragsSource;
        this.FragsList = null;
        this.PrimaryFragsList = null;
    }
    
    /**
     * Read the list of fragments from text file provided as local resource
     * (URL given in the constructor).
     * 
     * @throws Exception 
     */
    protected void InitFrags() throws Exception {
        // read frags list from the given resource
        URL u = getClass().getResource(this.FragsSource);
        this.FragsList = readFragmentsFromFile(u.openStream());
        this.PrimaryFragsList = new NRMEAFragments(getPrimaryFragments()); 
        initFragsList();
    }
    
    protected abstract void initFragsList() throws Exception;

    
    /**
     * Generic method to calculate the prediction, to be implemented in the subclass
     * for each specific model.
     * @param mol
     * @return
     * @throws Exception 
     */
    protected abstract int calculatePrediction(InsilicoMolecule mol) throws Exception; 
    
    
    /**
     * Return the prediction for a given SMILES structure, based on the 
     * parameters provided to the constructor. Calculation of the prediction
     * for the molecule relies on the class implemented in the specific
     * model subclass.
     * 
     * @param mol 
     * @return prediction category. inactive, chemicals are non-toxic; 
     * agonist, active chemicals can only mimic natural hormone’s action;
     * antagonist, active chemicals can only antagonize natural hormone’s action; 
     * A-Anta, active chemicals not only can mimic but also can antagonize natural hormone’s action.
     * @throws Exception 
     */
    public short Predict(InsilicoMolecule mol) throws Exception { 
    // dovrebbe restituire 0=inactive,1= agonist;2= antagonist, 3=A-anta
    //4= ND?
        int retval = 0; // inactive
    
        //System.out.println("Predict");
         // check if frags have been loaded
        if (this.FragsList == null)
            InitFrags();

        /* for log
        for (Fragment frag: this.FragsList){
            System.out.println(frag.toString());
        }
        */
        
        retval = calculatePrediction(mol); // if return ND, then inactive
        //System.out.println("retval: "+ retval);

        short pred = UNDEFINED;
        switch (retval) {
            case 1: pred = AGONIST;
                break;
            case 2: pred = ANTAGONIST;
                break;
            case 3: pred = A_ANTA;
                    break;
            default: pred = INACTIVE; // see if ND falls here or create another case
                break;
        }

        return pred;
    }
    
    
    /**
     * Return primary fragment list, based on the parameters provided 
     * to the constructor.
     * @return
     * @throws Exception 
     */
    protected ArrayList<Fragment> getPrimaryFragments() throws Exception{
        ArrayList<Fragment> primaryFragsList = new ArrayList<>();
        for (Fragment frag: this.FragsList){
            String st = frag.getType();
            if (st.equalsIgnoreCase(PRIMARY))
                primaryFragsList.add(frag);
        }
        
        if (primaryFragsList.isEmpty())
            throw new Exception("Primary fragment list is empty");
        
        return primaryFragsList;
    }
    
    protected ArrayList<Fragment> getFragmentListOSubType(String subtype) throws Exception{
        ArrayList<Fragment> fragsList = new ArrayList<>();
        for (Fragment frag: this.FragsList){
            String st = frag.getSubtype();
            if (st.equalsIgnoreCase(subtype))
                fragsList.add(frag);
        }
        
        if (fragsList.isEmpty())
            throw new Exception("Fragment list of type "+ subtype + " is empty");
        
        return fragsList;
    }
    
    
    /**
     * Read the list of fragments from the given resource.
      * 
     * @param source
     * @return
     * @throws Exception 
     */
    private static ArrayList<Fragment> readFragmentsFromFile(InputStream source) throws InitFailureException, IOException  {
        
        ArrayList<Fragment> FragsList = new ArrayList<>();
        DataInputStream in = new DataInputStream(source);
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        
        // read the first line from the text file
        String line = br.readLine();
            
        // loop until all lines are read
            while (line != null){
                
                // source file should be tab-separed plain text
                String[] data = line.split("\t");
                
                // source file should have id, fragment, type and subtype in 
                // in the first, second, third and fourth column, respectively
                Fragment frag = createFragment(data);
                               
                if (frag!= null) {
                    FragsList.add(frag);
                //System.out.println(frag);
                }
                
                line = br.readLine();
            }
        
        br.close();
        
        if (FragsList.isEmpty())
            throw new InitFailureException("Fragments list is empty");
        
        return FragsList;
    }
    
    
    /**
     * Create a single fragment, given the array of String data
     * @param data
     * @return
     * @throws Exception 
     */    
    private static Fragment createFragment(String[] data) throws InitFailureException {
       String id = data[0];
       String frag = data[1];
       String type = data[2];
       String subtype = data[3];
      
       return new Fragment(id, frag, type, subtype);
    }

}
