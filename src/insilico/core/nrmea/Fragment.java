package insilico.core.nrmea;

import insilico.core.exception.InitFailureException;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.isomorphism.matchers.QueryAtomContainer;
import org.openscience.cdk.smiles.smarts.parser.SMARTSParser;

/**
 *
 * @author Giovanna Lavado
 * @author Cosimo Toma
 */
public class Fragment {
    
    private final String id;
    private final String fragmentSMARTS;
    private final String type;
    private final String subtype;
    
    private final QueryAtomContainer query;
    

    public Fragment(String id, String fragmentSMARTS, String type, String subtype)
            throws InitFailureException {
        
        this.id = id;
        this.fragmentSMARTS = fragmentSMARTS;
        this.type = type;
        this.subtype = subtype;
        
        // build the QueryAtomContainer for the given SMARTS
        try {
            query = SMARTSParser.parse(fragmentSMARTS);
        } catch (CDKException e) {
            throw new InitFailureException("Unable to init SMARTS: " + fragmentSMARTS);
        }
    }

    
    public String getId() {
        return id;
    }

    public String getFragmentSMARTS() {
        return fragmentSMARTS;
    }

    public String getType() {
        return type;
    }

    public String getSubtype() {
        return subtype;
    }
    
    public QueryAtomContainer getQuery() {
        return query;
    }

    @Override
    public String toString(){
        return "Fragment[id "+ id + " frag " + fragmentSMARTS + " type " + type + " subtype " + subtype;
    }
    
    
}
